package com.servercommunication.filterobjects;

public class DepartmentFilter {
    private String name; 
    private String nameNe; 
    private String nameIn;
    private String nameLike; 
    private String nameIlike; 

    private DepartmentFilter[] and;
    private DepartmentFilter[] or;
    private DepartmentFilter not;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameNe() {
        return nameNe;
    }

    public void setNameNe(String nameNe) {
        this.nameNe = nameNe;
    }

    public String getNameIn() {
        return nameIn;
    }

    public void setNameIn(String nameIn) {
        this.nameIn = nameIn;
    }

    public String getNameLike() {
        return nameLike;
    }

    public void setNameLike(String nameLike) {
        this.nameLike = nameLike;
    }

    public String getNameIlike() {
        return nameIlike;
    }

    public void setNameIlike(String nameIlike) {
        this.nameIlike = nameIlike;
    }

    public DepartmentFilter[] getAnd() {
        return and;
    }

    public void setAnd(DepartmentFilter[] and) {
        this.and = and;
    }

    public DepartmentFilter[] getOr() {
        return or;
    }

    public void setOr(DepartmentFilter[] or) {
        this.or = or;
    }

    public DepartmentFilter getNot() {
        return not;
    }

    public void setNot(DepartmentFilter not) {
        this.not = not;
    }
}
