package com.servercommunication.mutationobjects;


import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name = "departmentDelete",
        arguments = {
                @GraphQLArgument(name = "departmentId")
        }
)
public class DepartmentDelete {
    private String departmentId;

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
}
