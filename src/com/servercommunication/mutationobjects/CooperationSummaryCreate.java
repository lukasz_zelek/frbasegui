package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.CooperationSummaryNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(
        name = "cooperationSummaryCreate",
        arguments = {
                @GraphQLArgument(name = "companyId"),
                @GraphQLArgument(name = "results"),
                @GraphQLArgument(name = "summary"),
                @GraphQLArgument(name = "year", type = "int")
        }
)
public class CooperationSummaryCreate {
    private CooperationSummaryNode cooperationSummary;

    public CooperationSummaryNode getCooperationSummary() {
        return cooperationSummary;
    }

    public void setCooperationSummary(CooperationSummaryNode cooperationSummary) {
        this.cooperationSummary = cooperationSummary;
    }
}
