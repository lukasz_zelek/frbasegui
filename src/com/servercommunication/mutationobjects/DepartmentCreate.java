package com.servercommunication.mutationobjects;

import com.servercommunication.graphobjects.DepartmentNode;
import io.aexp.nodes.graphql.annotations.GraphQLArgument;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;


@GraphQLProperty(
        name = "departmentCreate",
        arguments = {
                @GraphQLArgument(name = "name")
        }
)
public class DepartmentCreate {
    private DepartmentNode department;

    public DepartmentNode getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentNode department) {
        this.department = department;
    }
}
