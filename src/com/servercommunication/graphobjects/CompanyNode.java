package com.servercommunication.graphobjects;

import io.aexp.nodes.graphql.annotations.GraphQLProperty;

import java.util.ArrayList;

@GraphQLProperty(name="CompanyNode")
public class CompanyNode implements Node {
    private String name;
    private String email;
    private String www;
    private String phoneNumber;
    private int potential;
    private String description;
    private StaffNode responsiblePerson;
    private String id;
    private ContactHistoryNodeConnection contactHistory;
    private ContactPersonNodeConnection contactPersons;

    public ContactPersonNodeConnection getContactPersons() {
        return contactPersons;
    }

    public void setContactPersons(ContactPersonNodeConnection contactPersons) {
        this.contactPersons = contactPersons;
    }

    public ContactHistoryNodeConnection getContactHistory() {
        return contactHistory;
    }

    public void setContactHistory(ContactHistoryNodeConnection contactHistory) {
        this.contactHistory = contactHistory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPotential() {
        return potential;
    }

    public void setPotential(int potential) {
        this.potential = potential;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StaffNode getResponsiblePerson() {
        return responsiblePerson;
    }

    public void setResponsiblePerson(StaffNode responsiblePerson) {
        this.responsiblePerson = responsiblePerson;
    }

    @Override
    public String getId() {
        return id;
    }
}
