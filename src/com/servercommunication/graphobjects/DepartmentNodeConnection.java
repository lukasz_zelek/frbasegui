package com.servercommunication.graphobjects;

import java.util.ArrayList;

public class DepartmentNodeConnection {
    private PageInfo pageInfo;
    private ArrayList<DepartmentNodeEdge> edges;

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public ArrayList<DepartmentNodeEdge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<DepartmentNodeEdge> edges) {
        this.edges = edges;
    }
}
