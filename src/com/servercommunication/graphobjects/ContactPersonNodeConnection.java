package com.servercommunication.graphobjects;

import java.util.ArrayList;

public class ContactPersonNodeConnection {
    private PageInfo pageInfo;
    private ArrayList<ContactPersonNodeEdge> edges;

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public ArrayList<ContactPersonNodeEdge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<ContactPersonNodeEdge> edges) {
        this.edges = edges;
    }
}
