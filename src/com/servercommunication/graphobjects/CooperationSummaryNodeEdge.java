package com.servercommunication.graphobjects;

public class CooperationSummaryNodeEdge {
    private CooperationSummaryNode node;
    private String cursor;

    public CooperationSummaryNode getNode() {
        return node;
    }

    public void setNode(CooperationSummaryNode node) {
        this.node = node;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
