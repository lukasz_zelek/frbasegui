package com.servercommunication.graphobjects;

public class ContactHistoryNode implements Node {
    private String notes;
    private String contactDate;
    private StaffNode representative;
    private String id;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getContactDate() {
        return contactDate;
    }

    public void setContactDate(String contactDate) {
        this.contactDate = contactDate;
    }


    public StaffNode getRepresentative() {
        return representative;
    }

    public void setRepresentative(StaffNode representative) {
        this.representative = representative;
    }

    @Override
    public String getId() {
        return id;
    }
}
