package com.servercommunication.graphobjects;

public class ContactHistoryNodeEdge {
    private ContactHistoryNode node;
    private String cursor;

    public ContactHistoryNode getNode() {
        return node;
    }

    public void setNode(ContactHistoryNode node) {
        this.node = node;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
}
