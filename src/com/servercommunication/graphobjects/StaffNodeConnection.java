package com.servercommunication.graphobjects;

import java.util.ArrayList;

public class StaffNodeConnection {
    private PageInfo pageInfo;
    private ArrayList<StaffNodeEdge> edges;

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public ArrayList<StaffNodeEdge> getEdges() {
        return edges;
    }

    public void setEdges(ArrayList<StaffNodeEdge> edges) {
        this.edges = edges;
    }
}
