package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.StaffNode;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(name="me")
public class Me extends StaffNode{
}
