package com.servercommunication.queryobjects;

import com.servercommunication.graphobjects.CompanyNodeConnection;
import io.aexp.nodes.graphql.annotations.GraphQLProperty;

@GraphQLProperty(name="companiesAll")
public class CompaniesAll extends CompanyNodeConnection{
}
