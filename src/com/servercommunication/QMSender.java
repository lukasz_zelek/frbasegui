package com.servercommunication;

import io.aexp.nodes.graphql.*;
import io.aexp.nodes.graphql.internal.Error;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Klasa szablonowa na wysyłanie zapytań/mutacji. Nie ruszać składni, będzie prościej
 * @param <T> klasa reprezentująca mutację/zapytanie
 */
public class QMSender<T> {
    private GraphQLResponseEntity<T> responseEntity;

    /**
     * Podstawowy konstruktor, wysyła zapytanie. Wypisuje błędy do konsoli jeśli takowe się pojawią
     * @param qmClass obiekt klasowy klasy zapytania (tej samej co T)
     * @param arguments argumenty zapytania
     * @param authorise czy wysyłać token w nagłówku
     * @param isMutation jeśli true wysyła mutację, jeśli false zapytanie
     */
    public QMSender(Class<T> qmClass, Arguments arguments, boolean authorise, boolean isMutation){
        System.out.println(isMutation ? "Mutacja" : "Zapytanie");
        GraphQLTemplate graphQLTemplate = new GraphQLTemplate();
        Map<String,String > headers = new HashMap<>();
        if(authorise){
            headers = GlobalServerShit.getHeaders();
        }
        GraphQLRequestEntity requestEntity;
        try {
                requestEntity = GraphQLRequestEntity.Builder()
                        .url("http://api.xert.ct8.pl/graphiql")
                        .request(qmClass)
                        .headers(headers)
                        .arguments(arguments)
                        .build();
        } catch (MalformedURLException e) {
            this.responseEntity = null;
            System.out.println("Błąd URL");
            return;
        }
        System.out.println(requestEntity.toString());
        if(isMutation) this.responseEntity = graphQLTemplate.mutate(requestEntity, qmClass);
        else this.responseEntity = graphQLTemplate.query(requestEntity, qmClass);

        if(this.responseEntity.getErrors() != null){
            for(Error elem: this.responseEntity.getErrors()) System.out.println(elem.getMessage());
        }
    }

    /**
     * W sumie to samo co wyżej ale bez argumentów - tylko dla zapytań. Zawsze wysyła token
     * @param qmClass obiekt klasowy klasy zapytania (tej samej co T)
     */
    public QMSender(Class<T> qmClass){
        System.out.println("Zapytanie - noarg");
        GraphQLTemplate graphQLTemplate = new GraphQLTemplate();
        GraphQLRequestEntity requestEntity;
        try {
            requestEntity = GraphQLRequestEntity.Builder()
                    .url("http://api.xert.ct8.pl/graphiql")
                    .request(qmClass)
                    .headers(GlobalServerShit.getHeaders())
                    .build();
        } catch (MalformedURLException e) {
            this.responseEntity = null;
            System.out.println("Błąd URL");
            return;
        }
        this.responseEntity = graphQLTemplate.query(requestEntity, qmClass);
        if(this.responseEntity.getErrors() != null){
            for(Error elem: this.responseEntity.getErrors()) System.out.println(elem.getMessage());
        }
    }

    public GraphQLResponseEntity<T> getResponse() {return this.responseEntity;}
}
