package com.gui;

import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.StaffNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ChangePasswordFrame extends JFrame {
    ChangePasswordFrame(StaffNode me){
        super("Zmiana hasła");
        this.me = me;
        this.setIconImage(GlobalOptions.img.getImage());
        initGraphicalComponents();
    }

    private void initGraphicalComponents(){
        this.mainPanel = new JPanel();
        this.newPasswordPanel = new JPanel();
        this.repeatPasswordPanel = new JPanel();
        this.oldPasswordPanel = new JPanel();
        this.buttonPanel = new JPanel();

        this.newPasswordField = new JPasswordField();
        this.repeatPasswordField = new JPasswordField();
        this.oldPasswordField = new JPasswordField();

        this.saveButton = new JButton("Zapisz");
        this.cancelButton = new JButton("Anuluj");
        this.saveButton.addActionListener(this::actionPerformed);
        this.cancelButton.addActionListener(this::actionPerformed);

        this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.PAGE_AXIS));
        this.add(mainPanel);

        this.newPasswordPanel.setLayout(new BoxLayout(this.newPasswordPanel, BoxLayout.PAGE_AXIS));
        this.newPasswordPanel.add(new JLabel("Nowe hasło"));
        this.newPasswordPanel.add(this.newPasswordField);

        this.repeatPasswordPanel.setLayout(new BoxLayout(this.repeatPasswordPanel, BoxLayout.PAGE_AXIS));
        this.repeatPasswordPanel.add(new JLabel("Powtórz nowe hasło"));
        this.repeatPasswordPanel.add(this.repeatPasswordField);

        this.oldPasswordPanel.setLayout(new BoxLayout(this.oldPasswordPanel, BoxLayout.PAGE_AXIS));
        this.oldPasswordPanel.add(new JLabel("Stare hasło"));
        this.oldPasswordPanel.add(this.oldPasswordField);

        this.buttonPanel.setLayout(new GridLayout());
        this.buttonPanel.add(this.saveButton);
        this.buttonPanel.add(this.cancelButton);

        this.mainPanel.add(this.newPasswordPanel);
        this.mainPanel.add(this.repeatPasswordPanel);
        this.mainPanel.add(this.oldPasswordPanel);
        this.mainPanel.add(this.buttonPanel);

        this.pack();
    }

    private StaffNode me;

    private JPanel mainPanel;
    private JPanel newPasswordPanel;
    private JPanel repeatPasswordPanel;
    private JPanel oldPasswordPanel;
    private JPanel buttonPanel;

    private JPasswordField newPasswordField;
    private JPasswordField repeatPasswordField;
    private JPasswordField oldPasswordField;

    private JButton saveButton;
    private JButton cancelButton;

    public void actionPerformed(ActionEvent e){
        if(e.getSource() == this.cancelButton){
            dispose();
        }
        else if(e.getSource() == this.saveButton){
            String newPassword = String.valueOf(this.newPasswordField.getPassword());
            String oldPassword = String.valueOf(this.oldPasswordField.getPassword());
            String repeatPassword = String.valueOf(this.repeatPasswordField.getPassword());
            if(newPassword.equals(repeatPassword)){
                if(newPassword.contains("\\") || oldPassword.contains("\\")){
                    this.newPasswordField.setText("");
                    this.repeatPasswordField.setText("");
                    this.oldPasswordField.setText("");
                    EventQueue.invokeLater(WrongUserDataFrame::new);
                }
                else{
                    boolean ifSuccessful = ServerCommunication.mutations.staffChangePassword(this.me.getId(), newPassword, oldPassword);
                    if(ifSuccessful){
                        dispose();
                    }
                    else{
                        this.newPasswordField.setText("");
                        this.repeatPasswordField.setText("");
                        this.oldPasswordField.setText("");
                        EventQueue.invokeLater(WrongUserDataFrame::new);
                    }
                }


            }
            else{
                System.out.println("Nie są równe");
                this.newPasswordField.setText("");
                this.repeatPasswordField.setText("");
                this.oldPasswordField.setText("");
                EventQueue.invokeLater(WrongUserDataFrame::new);
            }
        }

    }
}
