package com.gui;

import com.servercommunication.GlobalServerShit;
import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.CompanyNode;
import com.servercommunication.graphobjects.ContactHistoryNode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Oki, to diabelstwo jest do wypisywania firm na liście. Zrobione tak by jedną pętlą po zasięgu
 * dało się zrobić całą listę. Zapamiętuje ID.
 *
 * TODO
 * listenery do przycisków by dało się firmy edytować i usuwać
 */
public class CompanyPanel extends JPanel implements ActionListener {
    public CompanyPanel(CompanyNode companyNode, MainFrame frame){
        this.id = companyNode.getId();
        this.setLayout(new GridLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.nameField.setText(companyNode.getName());
        this.emailField.setText(companyNode.getEmail());
        try{
            this.personField.setText(companyNode.getResponsiblePerson().getName() + " " + companyNode.getResponsiblePerson().getSurname());
        }catch (NullPointerException e){
            this.personField.setText("brak");
        }
        try{
            try{
                String dateString = companyNode.getContactHistory().getEdges().get(companyNode.getContactHistory().getEdges().size()-1).getNode().getContactDate();
                if(LocalDateTime.parse(dateString).getMinute() < 10){
                    dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":0" + LocalDateTime.parse(dateString).getMinute();
                }
                else{
                    dateString = LocalDateTime.parse(dateString).toLocalDate().toString() + ", " + LocalDateTime.parse(dateString).getHour() + ":" + LocalDateTime.parse(dateString).getMinute();
                }
                this.dateField.setText(dateString);
            }catch(IndexOutOfBoundsException a){
                this.dateField.setText("brak");
            }

        }catch (NullPointerException e){
            this.dateField.setText("brak");
        }

        this.initGraphicalComponents();

        node = companyNode;
        parentFrame = frame;
    }

    private void initGraphicalComponents(){
        this.add(namePanel);
        namePanel.add(nameLabel, BorderLayout.NORTH);
        namePanel.add(nameField, BorderLayout.SOUTH);
        nameField.setEditable(false);
        namePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.add(emailPanel);
        emailPanel.add(emailLabel, BorderLayout.NORTH);
        emailPanel.add(emailField, BorderLayout.SOUTH);
        emailField.setEditable(false);
        emailPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.add(personPanel);
        personPanel.add(personLabel, BorderLayout.NORTH);
        personPanel.add(personField, BorderLayout.SOUTH);
        personField.setEditable(false);
        personPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.add(datePanel);
        datePanel.add(dateLabel, BorderLayout.NORTH);
        datePanel.add(dateField, BorderLayout.SOUTH);
        dateField.setEditable(false);
        datePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.add(detailsButton);
        detailsButton.addActionListener(this);
        this.add(deleteButton);
        deleteButton.addActionListener(this);
    }

    private JLabel nameLabel = new JLabel("Nazwa");
    private JLabel emailLabel = new JLabel("E-mail");
    private JLabel personLabel = new JLabel("Osoba odpowiedzialna");
    private JLabel dateLabel = new JLabel("Data ostatniego kontaktu");

    private JTextField nameField = new JTextField();
    private JTextField emailField = new JTextField();
    private JTextField personField = new JTextField();
    private JTextField dateField = new JTextField();

    private JPanel namePanel = new JPanel(new BorderLayout());
    private JPanel emailPanel = new JPanel(new BorderLayout());
    private JPanel personPanel = new JPanel(new BorderLayout());
    private JPanel datePanel = new JPanel(new BorderLayout());

    private JButton detailsButton = new JButton("Szczegóły");
    private JButton deleteButton = new JButton("Usuń");

    private String id;

    private CompanyNode node;
    private MainFrame parentFrame;

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == deleteButton) {
            int ans = JOptionPane.showConfirmDialog(this,"Czy na pewno chcesz usunac firmę ?","Pytanie",JOptionPane.YES_NO_OPTION);
            if(ans == JOptionPane.YES_OPTION) {
                GlobalServerShit.communication.mutations.companyDelete(id);
                parentFrame.showMyCompanies();
            }
        } else if (e.getSource() == detailsButton) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new InputDataFrame(node).setVisible(true);
                    parentFrame.showMyCompanies();
                }
            });
        }
    }

}
