package com.gui;
import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatIntelliJLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.servercommunication.GlobalServerShit;
import com.servercommunication.ServerCommunication;
import com.servercommunication.graphobjects.StaffNode;

import javax.swing.*;
import java.awt.EventQueue;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Klasa inicjalizująca. Odpalaj nią program.
 * Może by to do oddzielnego pakietu przerzucić?
 */

public class InitClass {
    public static void main(String[] args) {
        //FIXME aby zmienić motyw ustaw tutaj wg https://www.formdev.com/flatlaf/themes/
        FlatDarculaLaf.install();
        Scanner tokenData = null;
        File tokenFile = new File("adds/token");
        File dirFile = new File("adds");
        try {
            tokenFile.createNewFile();
        } catch (IOException e) {
            dirFile.mkdir();
            try {
                tokenFile.createNewFile();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        try {
            tokenData = new Scanner(tokenFile);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        String authToken = "";
        if(tokenData.hasNext())
            authToken=tokenData.nextLine();
        if(authToken.equals(""))
            EventQueue.invokeLater(LoginFrame::new);
        else{
            GlobalServerShit.setMyToken(authToken.substring(7));
            System.out.println("Token remembered");
            StaffNode meNode = ServerCommunication.queries.me();
            if(meNode != null)
            {
                System.out.println("Logged in");
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        new MainFrame(meNode);
                    }
                });
            }
            else{
                EventQueue.invokeLater(LoginFrame::new);
            }
        }
    }
}
