package com.gui;

import com.servercommunication.graphobjects.StaffNode;

import javax.swing.*;

public class StaffGovFrame extends JFrame {
    StaffGovFrame(StaffNode me){
        super("Zarządzanie załogą");
        this.setIconImage(GlobalOptions.img.getImage());
        this.me = me;
    }

    private StaffNode me;
}
