package com.gui;
import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Takie globalne gówno na kolory, napisy i ikony GUI. Bo czemu by nie trzymać całego tego szajsu w jednej klasie
 */
public class GlobalOptions {
    public static Color userInputColor = new Color(150, 150, 150);
    public static Color buttonPanelBackgroundColor = new Color(200,200,200);
    public static Color buttonBackgroundColor = Color.GRAY;
    public static Color buttonForegroundColor = Color.BLACK;
    public static String superString = "ROBOCOMP FR";
    public static ImageIcon img = new ImageIcon("Woch_Wieslaw.jpg");
    public static int notePanelNoteLength = 40;

    public enum RANKS{
        MAIN_ORGANIZER,
        SECTION_COORDINATOR,
        NORMAL_WORKER,
        VOLUNTEER,
        ADMIN
    }

    public static Map<RANKS,String> rankToString= new HashMap<RANKS,String>(){{
        put(RANKS.MAIN_ORGANIZER,"MAIN_ORGANIZER");
        put(RANKS.SECTION_COORDINATOR,"SECTION_COORDINATOR");
        put(RANKS.NORMAL_WORKER,"NORMAL_WORKER");
        put(RANKS.VOLUNTEER,"VOLUNTEER");
        put(RANKS.ADMIN,"ADMIN");
    }};

}
