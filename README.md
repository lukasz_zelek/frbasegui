# Baza sponsorów Festiwalu Robotyki ROBOCOMP update 12.09.2020
- prosta w obsłudze aplikacja GUI
- do poprawy ciągle pozostaje aktualizowanie na bieżąco listy firm

Zrzut ekranu
![alt text](img/screenshot.png "Title Text")

Do działania wymagane jest środowisko Java w wersji 8 lub wyższej.

Aby uruchomić apkę należy dwuklikiem otworzyć jak standardowy program:
> jar/bazafrgui.jar

[Strona koła naukowego](http://www.integra.agh.edu.pl/)
